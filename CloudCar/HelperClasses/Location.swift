//
//  Location.swift
//  CloudCar
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class Location: NSObject {
    
    class func saveLat(Lat:String){
        let def = UserDefaults.standard
        def.setValue(Lat , forKey: "Lat")
        def.synchronize()
    }
    class  func getLat ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "Lat") as? String)
    }
    
    class func saveLong (Long:String){
        let def = UserDefaults.standard
        def.setValue(Long , forKey: "Long")
        def.synchronize()
    }
    class  func getLong ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "Long") as? String)
    }
    
}
