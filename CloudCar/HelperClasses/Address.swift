//
//  Address.swift
//  CloudCar
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

class Address: NSObject {
    
    class func saveAddressName (name:String){
        let def = UserDefaults.standard
        def.setValue(name , forKey: "name")
        def.synchronize()
    }
    class  func getAddressName ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "name") as? String)
    }
    
    class func saveAddress (address:String){
        let def = UserDefaults.standard
        def.setValue(address , forKey: "address")
        def.synchronize()
    }
    class  func getAddress ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "address") as? String)
    }
    
}
