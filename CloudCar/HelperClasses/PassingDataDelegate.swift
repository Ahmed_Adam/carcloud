//
//  PassingDataDelegate.swift
//  Kalemat
//
//  Created by mac on 6/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

protocol DataEnteredDelegate: class {
    func userDidEnterInformation(data: CarsRootClass)
}


