//
//  MyCar.swift
//  CloudCar
//
//  Created by mac on 7/3/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import UIKit

class MyCar: NSObject  {
    
    class func saveCarID(ID:Int){
        let def = UserDefaults.standard
        def.setValue(ID , forKey: "ID")
        def.synchronize()
    }
    class  func getCarID ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "ID") as? Int)
    }
    
    class func saveCarModel(carModel:String){
        let def = UserDefaults.standard
        def.setValue(carModel , forKey: "carModel")
        def.synchronize()
    }
    class  func getCarModel ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "carModel") as? String)
    }
    
    class func saveChassis_no(chassis_no:String){
        let def = UserDefaults.standard
        def.setValue(chassis_no , forKey: "chassis_no")
        def.synchronize()
    }
    class  func getChassis_no ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "chassis_no") as? String)
    }
    class func saveNumber_of_paintings(Number_of_paintings:String){
        let def = UserDefaults.standard
        def.setValue(Number_of_paintings , forKey: "Number_of_paintings")
        def.synchronize()
    }
    class  func getNumber_of_paintings ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "Number_of_paintings") as? String)
    }
    
    
    class func getCatImage(key: String) -> UIImage? {
        
        let imageData = UserDefaults.standard.value(forKey: "catImage")
        return UIImage(data: (imageData as? Data)!)!
       
    }
    class func setImage(image: UIImage?) {
        UserDefaults.standard.set(UIImageJPEGRepresentation(image!, 100), forKey: "catImage")
    }
    
}
