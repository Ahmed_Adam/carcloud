//
//  SaveUserID.swift
//  Kalemat
//
//  Created by mac on 5/22/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class SaveUserID: NSObject {
    
    class func saveUserID (id:Int){
        let def = UserDefaults.standard
        def.setValue(id , forKey: "id")
        def.synchronize()
    }
    class  func getUserID ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "id") as? Int)
    }
    
}

