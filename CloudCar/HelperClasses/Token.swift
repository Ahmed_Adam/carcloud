//
//  Token.swift
//  Kalemat
//
//  Created by mac on 5/22/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class Token: NSObject {
    
    class func saveAPIToken (token:String){
        let def = UserDefaults.standard
        def.setValue(token , forKey: "token")
        def.synchronize()
    }
    class  func getAPIToken ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "token") as? String)
    }
    
}

