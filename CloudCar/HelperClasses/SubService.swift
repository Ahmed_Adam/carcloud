//
//  SubService.swift
//  CloudCar
//
//  Created by mac on 7/3/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

class SubService: NSObject {
    
    class func saveSubServiceID (servicceID:Int){
        let def = UserDefaults.standard
        def.setValue(servicceID , forKey: "servicceID")
        def.synchronize()
    }
    class func saveServiceName (servicceName :String){
       let def = UserDefaults.standard
       def.setValue(servicceName , forKey: "servicceName")
       def.synchronize()
    }
     
    class  func getSubServiceID ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "servicceID") as? Int)
    }
    
    class  func getServiceName ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "servicceName") as? String)
    }

}
