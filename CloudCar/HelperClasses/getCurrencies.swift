//
//  getCurrencies.swift
//  Kalemat
//
//  Created by mac on 6/6/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

class Currencies: NSObject {
    
    class func saveCurrency (currency:String){
        let def = UserDefaults.standard
        def.setValue(currency , forKey: "currency")
        def.synchronize()
    }
    class  func getCurrency ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "currency") as? String)
    }
    
}

