//
//  FromSideMenu.swift
//  CloudCar
//
//  Created by mac on 7/4/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class FromSideMenu: NSObject {
    
    class func saveFromSideMenu (check :Bool){
        let def = UserDefaults.standard
        def.setValue(check , forKey: "check")
        def.synchronize()
    }
    class  func getFromSideMenu ()->Bool? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "check") as? Bool)
    }
    
}
