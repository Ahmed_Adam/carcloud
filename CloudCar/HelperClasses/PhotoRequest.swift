//
//  PhotoRequest.swift
//  Kalemat
//
//  Created by mac on 6/7/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class PhotoRequest {
    
     static let sharedInstance = PhotoRequest()
    
    func getPhoto (url: String , complition :   @escaping (_ error:Error? ,_ success: Bool , _ photo: UIImage ) -> Void){
        let  imageUrl = url
        Alamofire.request(imageUrl, method: .get).responseImage { response in
            guard let image = response.result.value else {
                // Handle error
                return
            }
            // Do stuff with your image
            complition(nil,true , image)
        }
        
        }
}

