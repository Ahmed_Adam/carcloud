//
//  getSubServices.swift
//  CloudCar
//
//  Created by mac on 7/12/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class SubServices: NSObject {
    
    class func saveSubServices (SubServices:String){
        let def = UserDefaults.standard
        def.setValue(SubServices , forKey: "SubServices")
        def.synchronize()
    }
    class  func getSubServices ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "SubServices") as? String)
    }
    
}
