//
//  workShop.swift
//  CloudCar
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import SwiftyJSON

class WorkShop: NSObject {
    
    //    "id": 2,
    //    "name": "2test",
    //    "adress": "2test",
    //    "winch": "1",
    //    "lat": "213213",
    //    "long": "23123",
    //    "city": "بصره",
    //
    //

    
    
    //  SAVE THE WORKSHOP INFO //
    class func saveWorkShop (workshop:Int){
   
        let def = UserDefaults.standard
        def.setValue(workshop , forKey: "workshop")
        def.synchronize()
    }
    
    class func saveWorkShopName (workshopName:String){
        let def = UserDefaults.standard
        def.setValue(workshopName , forKey: "workshopName")
        def.synchronize()
    }
    
    class func saveWorkShopAddress (workshopAddress:String){
        let def = UserDefaults.standard
        def.setValue(workshopAddress , forKey: "workshopAddress")
        def.synchronize()
    }
 
    class func saveWorkShopWinch (workshopWinch:String){
        let def = UserDefaults.standard
        def.setValue(workshopWinch , forKey: "workshopWinch")
        def.synchronize()
    }
    class func saveWorkShopLat (workshopLat:String){
        let def = UserDefaults.standard
        def.setValue(workshopLat , forKey: "workshopLat")
        def.synchronize()
    }
    class func saveWorkShopLong (workshopLong:String){
        let def = UserDefaults.standard
        def.setValue(workshopLong , forKey: "workshopLong")
        def.synchronize()
    }
    class func saveWorkShopCity (workshopCity:String){
        let def = UserDefaults.standard
        def.setValue(workshopCity , forKey: "workshopCity")
        def.synchronize()
    }
    
    
    // GET WORKSHOP INFO //
    
    class  func getWorkShop ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "workshop") as? Int)
    }
    
    class  func getWorkShopName ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "workshopName") as? String)
    }
    
    class  func getWorkShopAddress ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "workshopAddress") as? String)
    }
    
    class  func getWorkShopWinch ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "workshopWinch") as? String)
    }
}
