//
//  UsersAPIs.swift
//  CloudCar
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//



import Foundation
import Alamofire
import SwiftyJSON
class UserAPIs{
    
    static let sharedInstance = UserAPIs()
    
    //    let language = CheckLanguage.getCheckLanguage()
    var url :String!
    var language = CheckLanguage.getCheckLanguage()
    //******** Login Request ******** //
    
    
    func login (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ LogInData : LoginResponse) -> Void){
        
        url = URLs.login_ar
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json"  ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)

           
                let dataFromJson  =  LoginResponse(json: jsonString)
            if dataFromJson.code == 200 {
                Token.saveAPIToken(token: dataFromJson.data.token)
                print("\(dataFromJson.data.token)")
            }
            complition(nil, true, dataFromJson)
       
        }
    }
    
    
    
    //   **************************************************************** //
    
    
    //******** Register Request ******** //
    

    func register (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ regsterModel:RegisterResponse ) -> Void){
        //        if language == "en"{
        //            url = URLs.register_en
        //        }
        //        else if language == "ar"{
        url = URLs.register_ar
        //        }

        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json"  ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers


        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  RegisterResponse(json: jsonString)
            Token.saveAPIToken(token: dataFromJson.data.token)
            complition(nil, true, dataFromJson)
            
        }
    }




        // **************************************************************** //
    
        //********   Activation code  Request ******** //
    
    
        func ActivationCode (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ acivationcode : ActivateCodeResponse) -> Void){
    
//
//            if language == "en"{
                url = URLs.activate_user_en
//            }
//            else if language == "ar"{
//                url = URLs.resend_activation_code_ar
//            }
            let parameters = info
            let urlComponent = URLComponents(string: url)!
            let token = Token.getAPIToken()
            let headers = [ "Content-Type": "application/json"  ,
                            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdkMmIzNWIyYmRiMWE4ZDhiYmY2YTg1YzEwMjQ4ZTFmNzA3MTMwZDdiMmJmNDQxOTY4NWYwNzk0ZTcyMjk2ZmFlODE1NGIyMTliZWY3YjA3In0.eyJhdWQiOiIxIiwianRpIjoiN2QyYjM1YjJiZGIxYThkOGJiZjZhODVjMTAyNDhlMWY3MDcxMzBkN2IyYmY0NDE5Njg1ZjA3OTRlNzIyOTZmYWU4MTU0YjIxOWJlZjdiMDciLCJpYXQiOjE1MzE2NTE4MDcsIm5iZiI6MTUzMTY1MTgwNywiZXhwIjoxNTYzMTg3ODA3LCJzdWIiOiIyNyIsInNjb3BlcyI6W119.AZshOf43BKQcYx_C2a1zbM4GAKe_0LJxPW40AvDLU233HEVTxOtzKky5_hGrgkewYZt3uujtfcF0McP8xjscBZ9h-kdFh20OUExbvQQSlEKt1sgTxJ4DfhEc5n-gMVPZn6ktCt7Smn9TS8Umjl_DIxCIMBms2QTvXMFDCG5UnUesPot4dCE2AjUMlzLSjxUWVwDJuPND0HzqRT7rCHfQeibniLWggb2tHhhYE8ZOFsmWiI0LVedoDarINltFrjyBhmdIjPdOAP5bXDWMhgB7b_yGL00B1teLF5A7lfTTCPUQgA9RRwZcBC_bzPDpuhBwK8HUsny9q4TrIkhU63h3QyWvTUVEI-aBwQte7JniJdRa7De7zx7myUuVygJWlHdPxiiiBd7FCuWrezemQMx8F6MiApMBCYNgtlt-3YZ1TSxTlRHKnQt0DSCHytmAMZvmKxaRfz1qUckIkYump0hwzDkrkA1qKDfOHuaF6SN7KvhLx3NpeoJ57qA8CTgR1clqgkJivYokMbls5CUkr2FDFe9tk6p-B6mFzFUO-m80tV5kYlDXz1SK0xI-HVFYXMx__BHcZvoKxHYE_JLpgz5kilGl62WkXFpVrCL1pI_G0ee3TE4BNPXeittQ2ei6jPIlCJaZbKKsBfWnDER5DbF8h2q7I-I8SFRhc5wicawD_kQ" ,
                            "X-Requested-With" : "XMLHttpRequest",
                            "language": language!]
            var request = URLRequest(url: urlComponent.url!)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            request.allHTTPHeaderFields = headers
    
            //***** Alamofire request *****//
            Alamofire.request(request).responseJSON { (response ) in
                
                let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
                let dataFromJson  =  ActivateCodeResponse(json: jsonString)
                
                complition(nil, true, dataFromJson)
                
            }
        }
    
    
        // **************************************************************** //
    
    //
    //    //********  Resend  Request ******** //
    //
    //
    //    func ForgetPassword  (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ forgetPassword : RegisterModel) -> Void){
    //
    //
    //        if language == "en"{
    //            url = URLs.forget_password_en
    //        }
    //        else if language == "ar"{
    //            url = URLs.forget_password_ar
    //        }
    //        let parameters = info
    //        let urlComponent = URLComponents(string: url)!
    //        let headers = [ "Content-Type": "application/json" ]
    //        var request = URLRequest(url: urlComponent.url!)
    //        request.httpMethod = "POST"
    //        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
    //        request.allHTTPHeaderFields = headers
    //
    //        //***** Alamofire request *****//
    //        Alamofire.request(request).responseData { (response) in
    //
    //            switch response.result {
    //
    //            case .success( let data ):
    //
    //                let jsonString = data
    //                //***** Data Returned *****//
    //                let dataFromJson = try? JSONDecoder().decode(RegisterModel.self, from: jsonString)
    //                let code = dataFromJson?.code
    //                complition(nil , true , code! , dataFromJson!)
    //
    //
    //            case .failure (let error):
    //                print(error)
    //
    //
    //            }
    //        }
    //    }
    //
    //    // **************************************************************** //
    //
    //
    //    //******** GetUserProfile ******** //
    //
    //
    //    func GetUserProfile ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ userProfile : UserProfile) -> Void){
    //
    //        let token = Token.getAPIToken()
    //
    //
    //        if language == "en"{
    //            url = URLs.get_user_profile_en + "?api_token=" + "\(token!)"
    //        }
    //        else if language == "ar"{
    //            url = URLs.get_user_profile_ar + "?api_token=" + "\(token!)"
    //        }
    //
    //        let urlComponent = URLComponents(string: url)!
    //        let headers = [ "Content-Type": "application/json" ]
    //        var request = URLRequest(url: urlComponent.url!)
    //        request.httpMethod = "GET"
    //        request.allHTTPHeaderFields = headers
    //
    //        //***** Alamofire request *****//
    //        Alamofire.request(request).responseData  { (response) in
    //            switch response.result
    //            {
    //            case .failure(let error):
    //                print(error)
    //            case .success( let data ):
    //
    //                let jsonString = data
    //                //***** Data Returned *****//
    //                let dataFromJson = try? JSONDecoder().decode(UserProfile.self, from: jsonString)
    //
    //                complition(nil , true  , dataFromJson!)
    //
    //
    //
    //            }
    //        }
    //    }
    //
    //    // **************************************************************** //
    //
    //    //******** GetCountry ******** //
    //
    //
    //    func GetCountry ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ userProfile : [CountryModel]) -> Void){
    //
    //
    //
    //        if language == "en"{
    //            url = URLs.get_countrys_en
    //        }
    //        else if language == "ar"{
    //            url = URLs.get_countrys_ar
    //        }
    //        let urlComponent = URLComponents(string: url)!
    //        let headers = [ "Content-Type": "application/json" ]
    //        var request = URLRequest(url: urlComponent.url!)
    //        request.httpMethod = "GET"
    //        request.allHTTPHeaderFields = headers
    //
    //        //***** Alamofire request *****//
    //        Alamofire.request(request).responseData  { (response) in
    //            switch response.result
    //            {
    //            case .failure(let error):
    //                print(error)
    //            case .success( let data ):
    //
    //                let jsonString = data
    //                //***** Data Returned *****//
    //                let dataFromJson = try? JSONDecoder().decode([CountryModel].self, from: jsonString)
    //
    //                complition(nil , true  , dataFromJson!)
    //
    //
    //
    //            }
    //        }
    //    }
    //
    //    // **************************************************************** //
    //
    //    //******** GetCountry ******** //
    //
    //
    //    func getContactData ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ userProfile : UserInfo) -> Void){
    //
    //
    //        let url = URLs.get_contact_data_en
    //
    //        let urlComponent = URLComponents(string: url)!
    //        let headers = [ "Content-Type": "application/json" ]
    //        var request = URLRequest(url: urlComponent.url!)
    //        request.httpMethod = "GET"
    //        request.allHTTPHeaderFields = headers
    //
    //        //***** Alamofire request *****//
    //        Alamofire.request(request).responseData  { (response) in
    //            switch response.result
    //            {
    //            case .failure(let error):
    //                print(error)
    //            case .success( let data ):
    //
    //                let jsonString = data
    //                //***** Data Returned *****//
    //                let dataFromJson = try? JSONDecoder().decode(UserInfo.self, from: jsonString)
    //
    //                complition(nil , true  , dataFromJson!)
    //
    //
    //
    //            }
    //        }
    //    }
    //
    //    //**************************************************************** //
    //
    //
    //
    //    //********  submit_contact_form ******** //
    //
    //
    //    func submitContactForm  (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ code:Int , _ repponce : ResponceModel) -> Void){
    //
    //
    //        if language == "en"{
    //            url = URLs.submit_contact_form_en
    //        }
    //        else if language == "ar"{
    //            url = URLs.submit_contact_form_ar
    //        }
    //        let parameters = info
    //        let urlComponent = URLComponents(string: url)!
    //        let headers = [ "Content-Type": "application/json" ]
    //        var request = URLRequest(url: urlComponent.url!)
    //        request.httpMethod = "POST"
    //        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
    //        request.allHTTPHeaderFields = headers
    //
    //        //***** Alamofire request *****//
    //        Alamofire.request(request).responseData { (response) in
    //
    //            switch response.result {
    //
    //            case .success( let data ):
    //
    //                let jsonString = data
    //                //***** Data Returned *****//
    //                let dataFromJson = try? JSONDecoder().decode(ResponceModel.self, from: jsonString)
    //                let code = dataFromJson?.code
    //                complition(nil , true , code! , dataFromJson!)
    //
    //
    //            case .failure (let error):
    //                print(error)
    //            }
    //        }
    //    }
    //
    //    // **************************************************************** //
    //
    //
    
    
}

