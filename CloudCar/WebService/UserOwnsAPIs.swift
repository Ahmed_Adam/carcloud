//
//  UserOwnsAPIs.swift
//  CloudCar
//
//  Created by mac on 7/2/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//



import Foundation
import Alamofire
import SwiftyJSON

class UserOwnsAPIs{
    
    static let sharedInstance = UserOwnsAPIs()
    
    let language = CheckLanguage.getCheckLanguage()
    var url :String!
    let token = Token.getAPIToken()
    
    
    //******** GetUserProfile ******** //
    
    
    func GetUserCars ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ cats : CarsRootClass) -> Void){
        
        url = URLs.get_UserCars
        
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "token" : token! ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  CarsRootClass(json: jsonString)
            
            complition(nil, true, dataFromJson)
        }
    }
    
    //******** add car Request ******** //
    
    
    func addCar (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ carData : AddCarResponseRoot) -> Void){
        
        url = URLs.addCar
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json"  ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!,
                        "token": token!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            
            
            let dataFromJson  =  AddCarResponseRoot(json: jsonString)
            if dataFromJson.code == 200 {
                print("WOoooooooooOooooooooooOW")
            }
            complition(nil, true, dataFromJson)
            
        }
    }
    
    
    //******** edit  car Request ******** //
    
    
    func editCar (carID:Int , info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ carData : AddCarResponseRoot) -> Void){
        
        url = URLs.editCar + "\(carID)"
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json"  ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!,
                        "token": token!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            
            
            let dataFromJson  =  AddCarResponseRoot(json: jsonString)
            if dataFromJson.code == 200 {
                print("WOoooooooooOooooooooooOW")
            }
            complition(nil, true, dataFromJson)
            
        }
    }
    
    
    //******** DestroyCars ******** //
    
    
    func DestroyCar ( carID:Int , complition :   @escaping (_ error:Error? ,_ success: Bool  , _ result : DestroyCarModel) -> Void){
        
        url = URLs.destroyCar + "\(carID)"
        
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "token" : token! ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  DestroyCarModel(json: jsonString)
            
            complition(nil, true, dataFromJson)
        }
    }
    
    
    func GetMyOrders (  complition :   @escaping (_ error:Error? ,_ success: Bool  , _ result : OrdersRoot) -> Void){
        
        url = URLs.allOrders
        
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!,
                        "token":token!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  OrdersRoot(json: jsonString)
            
            complition(nil, true, dataFromJson)
        }
    }
    
    
    //******** add car Request ******** //
    
    
    func storeOrder (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ order : MyDatorahPayment) -> Void){
        
        let parameters = info
        
        url = URLs.storeOrder
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!,
                        "token": token!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            
            
            let dataFromJson  =  MyDatorahPayment(json: jsonString)
            if dataFromJson.code == 200 {
                print("WOoooooooooOooooooooooOW")
            }
            complition(nil, true, dataFromJson)
            
        }
    }
    
}

