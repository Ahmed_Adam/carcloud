//
//  ServicesAPI.swift
//  CloudCar
//
//  Created by mac on 7/2/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//



import Foundation
import Alamofire
import SwiftyJSON

class ServicesAPI{
    
    static let sharedInstance = ServicesAPI()
    let token = Token.getAPIToken()
    let language = CheckLanguage.getCheckLanguage()
    var url :String!
    
    //******** GetUserProfile ******** //
    
    
    func GetServiceCategories ( complition :   @escaping (_ error:Error? ,_ success: Bool  , _ cats : CatRootsModel) -> Void){

        url = URLs.get_api_cats_ar
      
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  CatRootsModel(json: jsonString)
            complition(nil, true, dataFromJson)
        }
    }
    
    //******** GetSubCategories ******** //

    func GetSubCategories ( catID : Int ,complition :   @escaping (_ error:Error? ,_ success: Bool  , _ cats : SubCatRootModel) -> Void){
        
        
       // url = URLs.get_SubCats + "\(catID)"
        url = URLs.get_SubCats + "\(catID)/-1"
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  SubCatRootModel(json: jsonString)
            complition(nil, true, dataFromJson)
        }
    }
    
    
    //******** getServices Request ******** //
    
    
    func getServices (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ services : ServicesRoot) -> Void){
        
        url = URLs.services
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  ServicesRoot(json: jsonString)
            complition(nil, true, dataFromJson)
            
        }
    }
    
    
    
    func GetSubService ( catID : Int ,complition :   @escaping (_ error:Error? ,_ success: Bool  , _ cats : SubCatRootModel) -> Void){
        
        
        url = URLs.get_SubCats + "\(catID)"
        
        
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            let dataFromJson  =  SubCatRootModel(json: jsonString)
            complition(nil, true, dataFromJson)
        }
    }
    
    //******** Get workShop  ******** //
    
    
    func GetWorkShop (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ workshop : WorkshopRoot) -> Void){
        
        url = URLs.workshop
        let parameters = info
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json"  ,
                        "X-Requested-With" : "XMLHttpRequest",
                        "language": language!]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        
        request.allHTTPHeaderFields = headers
        
        //***** Alamofire request *****//
        
        
//        "name_ar:"ssss", "adress_ar":"eeeeeee", "name_en":"65656", "adress_en":"65656", "service_id":"65656", "cat_ar_id":"65656", "cat_en_id":"65656", "city_id":"11", "langues"["ar",'en'],
        
        
        if info["service_id"] == "" || info["cat_ar_id"] == "" ||  info["cat_en_id"] == "" {
           request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            Alamofire.request(request).responseJSON { (response ) in
                
                let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
                
                
                let dataFromJson  =  WorkshopRoot(json: jsonString)
                if dataFromJson.code == 200 {
                    print("WOoooooooooOooooooooooOW")
                }
                complition(nil, true, dataFromJson)
                
            }
        }
        else {
            
            Alamofire.request(request).responseJSON { (response ) in
                
                let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
                
                
                let dataFromJson  =  WorkshopRoot(json: jsonString)
                if dataFromJson.code == 200 {
                    print("WOoooooooooOooooooooooOW")
                }
                complition(nil, true, dataFromJson)
                
            }
        }
    }
    
    
}
