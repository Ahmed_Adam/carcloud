//
//  URLs.swift
//  CloudCar
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//


import Foundation
struct URLs {
    
    static let baseURL = "http://cloud.easydevs.com/"
    
    //******** Login url ******** //
    static let login_en =   URLs.baseURL + "api/login"
    static let login_ar =   URLs.baseURL + "api/login"
    
    //******** register url ******** //
    static let register_en = URLs.baseURL + "api/register"
    static let register_ar = URLs.baseURL + "api/register"
    
    //******** activate_user ******** //
    static let activate_user_en = URLs.baseURL + "api/user/code"
    static let activate_user_ar = URLs.baseURL + "api/user/code"
    
    //******** resend_activation_code ******** //
    static let resend_activation_code_en = URLs.baseURL + "resend_activation_code/"
    static let resend_activation_code_ar = URLs.baseURL + "resend_activation_code/"
    
    //******** forget_password ******** //
    static let forget_password_en = URLs.baseURL + "forget_password/"
    static let forget_password_ar = URLs.baseURL + "forget_password/"
    
    //******** change_forget_password ******** //
    static let change_forget_password_en = URLs.baseURL + "change_forget_password/"
    static let change_forget_password_ar = URLs.baseURL + "change_forget_password/"
    
    //******** change_password ******** //
    static let change_password_en = URLs.baseURL + "change_password/"
    static let change_password_ar = URLs.baseURL + "change_password/"
    
    //******** get_api_cats ******** //
    static let get_api_cats_en = URLs.baseURL + "api/cats"
    static let get_api_cats_ar = URLs.baseURL + "api/cats"
    
    //******** get_api_cats ******** //
    static let get_UserCars = URLs.baseURL + "api/cars"
    
    
    static let get_SubCats = URLs.baseURL + "api/service/"
    
    static let addCar = URLs.baseURL + "api/cars/store"
    static let editCar = URLs.baseURL + "api/cars/update/"
    
    static let destroyCar = URLs.baseURL + "api/cars/destroy/"
    static let services = URLs.baseURL + "api/cat/service"
    
    static let workshop = URLs.baseURL + "api/workshop"
    
    static let allOrders =  URLs.baseURL + "api/allOrders"
    static let storeOrder = URLs.baseURL + "api/order/store"
}
