//
//  ReportViewController.swift
//  CloudCar
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) 
        return cell
    }
    
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var bookDateView: UIView!
    @IBOutlet weak var serviceDateView: UIView!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropShadow()
        
        tableView.delegate = self
        tableView.dataSource = self
        setBorders(view: orderView)
        setBorders(view: bookDateView)
        setBorders(view: serviceDateView)
        setBorders(view: locationView)
    }
    func dropShadow(){
       orderView.dropShadow()
        bookDateView.dropShadow()
        serviceDateView.dropShadow()
        locationView.dropShadow()
    }
    func setBorders(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
    }

}
