//
//  MyOrdersViewController.swift
//  CloudCar
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class MyOrdersViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    var orders :OrdersRoot!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! orderTableViewCell
        cell.awakeFromNib()
//        cell.report.tag = indexPath.row
//        cell.report.superview?.tag = indexPath.section
//        
//        cell.report.addTarget(self, action: #selector(report(_:)), for: .touchUpInside)
//        cell.brandName.text = orders.data[indexPath.row].car.cat.name
//        cell.brandModel.text = orders.data[indexPath.row].car.sub_cat.name
//        cell.modelYear.text = "\(orders.data[indexPath.row].car.year)"
//        cell.general.text = "status :  " + orders.data[indexPath.row].status
        

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let report = self.storyboard?.instantiateViewController(withIdentifier: "MakeReport")
        self.show(report!, sender: self)
    }
    
    @objc func report(_ sender: UIButton) {
        
        let report = self.storyboard?.instantiateViewController(withIdentifier: "ReportCell")
        self.show(report!, sender: self)
        
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         getAllOrders()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        getAllOrders()
        
    }

    func getAllOrders(){
        
        UserOwnsAPIs.sharedInstance.GetMyOrders { (error , success , Orders ) in
            self.orders = Orders
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
}
