//
//  MakeReportViewController.swift
//  CloudCar
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class MakeReportViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropShadow()
        
        
    }
    func dropShadow(){
        machineView.dropShadow()
        airConditioningView.dropShadow()
        electricity.dropShadow()
        machine.dropShadow()
    }
    
    @IBOutlet weak var machineView: UIView!
    @IBOutlet weak var airConditioningView: UIView!
    @IBOutlet weak var electricity: UIView!
    @IBOutlet weak var machine: UIView!
    
    
    @IBAction func drying(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    
}
