//
//  orderTableViewCell.swift
//  CloudCar
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class orderTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var carLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var report: UIButton!
    @IBOutlet weak var general: UILabel!
    
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var brandModel: UILabel!
    @IBOutlet weak var modelYear: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(sender:)))
        swipe.direction = .left
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
        
    }
    
    @objc func handleSwipe (sender: UISwipeGestureRecognizer) {
        carLogoConstraint.constant = 20
        report.isHidden = false

        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleRightSwipe(sender:)))
        swipe.direction = .right
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
    }
    
    @objc func handleRightSwipe (sender: UISwipeGestureRecognizer) {
            carLogoConstraint.constant = 70
            report.isHidden = true
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(sender:)))
        swipe.direction = .left
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
    }


}
