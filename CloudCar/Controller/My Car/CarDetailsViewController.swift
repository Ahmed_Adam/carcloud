//
//  CarDetailsViewController.swift
//  CloudCar
//
//  Created by mac on 7/3/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import DropDown

class CarDetailsViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var chasisID: UITextField!
    @IBOutlet weak var numberPainings: UITextField!
    @IBOutlet weak var ModelYear: UITextField!
        @IBOutlet weak var brandOutlet: UIButton!
    @IBOutlet weak var modelCar: UIButton!
    
    var cats: CatRootsModel!
    var subCat: SubCatRootModel!
    var carOFCats = [CategoriesModel]()
    var subCatCars = [SubCategoriesData]()
    var carModels = [String]()
    var subCatModel = [String]()
    var catsIDs = [Int]()
    
    var catID : Int!
    var subCatID: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

    @IBAction func selectBrand(_ sender: UIButton) {
        
        
        
        
        let dd = DropDown()
        dd.anchorView = (sender as AnchorView)
        dd.dataSource = carModels
        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
        dd.selectionAction = {[unowned self](index : Int , item : String)in
            sender.setTitle(item, for: .normal)
            self.getSubCat(id: self.catsIDs[index])
            self.catID = self.catsIDs[index]
        }
        dd.show()
        
    }
    
    @IBAction func selectModel(_ sender: UIButton) {
        
        
        let dd = DropDown()
        dd.anchorView = (sender as AnchorView)
        dd.dataSource = subCatModel
        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
        dd.selectionAction = {[unowned self](index : Int , item : String)in
            sender.setTitle(item, for: .normal)
            self.subCatID = self.subCatCars[index].id
        }
        dd.show()
        
    }
    
    func getSubCat(id : Int){
        ServicesAPI.sharedInstance.GetSubCategories(catID: id) { (error , success , subCat) in
            self.subCat = subCat
            self.subCatCars = subCat.data
            self.subCatModel.removeAll()
            for item in self.subCatCars{
                self.subCatModel.append(item.name)
                
            }
        }
    }
    

    
    var fromNew :Bool!
    override func viewWillAppear(_ animated: Bool) {
//        if fromNew ==  true {
            edit.isEnabled = false
            edit.isHidden = true
            submit.isEnabled = true
            submit.isHidden = false
            carModels.removeAll()
            subCatModel.removeAll()
            catsIDs.removeAll()
            getCats()
//        }
//        else {
//
//            modelCar.setTitle(MyCar.getCarModel(), for: .normal)
//            self.chasisID.text = MyCar.getChassis_no()
//            self.numberPainings.text = MyCar.getNumber_of_paintings()
//
//            submit.isEnabled = false
//            submit.isHidden = true
//            edit.isEnabled = true
//            edit.isHidden = false
//            getCats()
//        }
       
        
    }
    func getCats(){
        ServicesAPI.sharedInstance.GetServiceCategories { (error , success , cats ) in
            self.cats = cats
            for item in cats.data{
                self.carModels.append(item.name)
                self.catsIDs.append(item.id)
            }
            
        }
    }
    
    
    @IBAction func UpdateCAr(_ sender: UIButton) {
        
        self.chasisID.isEnabled = false
        
        let info = [
                    "Number_of_paintings":numberPainings.text!,
                    "subCat_id": "\(self.subCatID!)",
                    "year": self.ModelYear.text! ,
                    "cat_id": "\(self.catID!)"
        ]

    }
    
    
    
    
    @IBAction func addCar(_ sender: UIButton) {
        if self.subCatID != nil || self.catID != nil {
            let info = ["chassis_no": chasisID.text!,
                        "Number_of_paintings":numberPainings.text!,
                        "subCat_id": "\(self.subCatID!)",
                "year": self.ModelYear.text! ,
                "cat_id": "\(self.catID!)"
            ]
            
            UserOwnsAPIs.sharedInstance.addCar(info: info) { (error , success , carsReponse ) in
                if carsReponse.check == true && carsReponse.code == 200{
                    let alert = UIAlertController(title: "Alert", message: " Car Added!  " , preferredStyle: UIAlertControllerStyle.alert)
                    
                    self.present(alert, animated: true, completion: nil)
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
                        // your code with delay
                        alert.dismiss(animated: true, completion: nil)
                        
                    }
                    
                }
                else {
                    let alert = UIAlertController(title: "Alert", message: " Car couldn't be Added! \n check car information   " , preferredStyle: UIAlertControllerStyle.alert)
                    
                    self.present(alert, animated: true, completion: nil)
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
                        // your code with delay
                        alert.dismiss(animated: true, completion: nil)
                        
                    }
                }
            }
        }
        else {
            let alert = UIAlertController(title: "Alert", message: " Car couldn't be Added! \n check car information   " , preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                // your code with delay
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    
}

