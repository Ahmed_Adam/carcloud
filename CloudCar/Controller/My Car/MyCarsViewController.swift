//
//  MyCarsViewController.swift
//  CloudCar
//
//  Created by Adam on 6/14/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class MyCarsViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    
    
    var car: CarsData!
    var fromMenu = false
    var cars : CarsRootClass!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getCars()
    }
    override func viewWillAppear(_ animated: Bool) {
        getCars()
    }
    
    func getCars(){
        UserOwnsAPIs.sharedInstance.GetUserCars { (error, success , cars ) in
            self.cars = cars
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @objc func back(_ sender: UIButton) {
        
        let editCar = self.storyboard?.instantiateViewController(withIdentifier: "EditCar")
        self.show(editCar!, sender: self)
        
    }
    
    @objc func editButton(_ sender: UIButton) {
        
        
        if self.cars.code != 400{
            let index = sender.tag
            let carId = self.cars.data[index].id
            
            MyCar.saveCarID(ID: carId)
                    let editCar = self.storyboard?.instantiateViewController(withIdentifier: "EditCar") as!CarDetailsViewController
            editCar.fromNew = false
            self.show(editCar, sender: self)
                }
    }
    @objc func deleteButton(_ sender: UIButton) {
        if self.cars.code != 400{
        let index = sender.tag
        let carId = self.cars.data[index].id
        let alert = UIAlertController(title: "Alert", message: " Are you sure remove this car ?!  " , preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                UserOwnsAPIs.sharedInstance.DestroyCar(carID: carId, complition: { (error , success , DestroyResult) in
                    self.getCars()
                    self.collectionView.reloadData()
                    
                })
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    func collectionView(_ catCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.cars.code != 400{
        let count = self.cars.data.count
        return count
        }
        return 0 
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(collectionView.frame.size.width) , height: CGFloat(85))
    }
    
    func collectionView(_ catCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =   collectionView.dequeueReusableCell(withReuseIdentifier: "myCars", for: indexPath) as! MyCarsCollectionViewCell
        cell.containerView.layer.borderWidth = 2
        cell.containerView.layer.borderColor = UIColor.lightGray.cgColor
        cell.containerView.dropShadow()
        cell.editButton.tag = indexPath.row
        cell.editButton.superview?.tag = indexPath.section
        cell.editButton.addTarget(self, action: #selector(editButton(_:)), for: .touchUpInside)
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.superview?.tag = indexPath.section
        cell.deleteButton.addTarget(self, action: #selector(deleteButton(_:)), for: .touchUpInside)
        if self.cars.code != 400{
        cell.modelYear.text = "\(self.cars.data[indexPath.row].year)"
        cell.cat.text = self.cars.data[indexPath.row].cat.name
        cell.series.text = self.cars.data[indexPath.row].sub_cat.name
        }
        //        car = self.cars.data[indexPath.row]
        //        MyCar.saveCar(car: car)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let fromsideMenu = FromSideMenu.getFromSideMenu()
        if fromsideMenu == false {
            car = self.cars.data[indexPath.row]
            MyCar.saveCarModel(carModel: car.cat.name)
            MyCar.saveCarID(ID: car.id)
            PhotoRequest.sharedInstance.getPhoto(url: car.cat.imageUrl) { (error, success, image) in
                MyCar.setImage(image: image )
            }
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
            self.present(linkingVC, animated: true, completion: nil)
        }
            
            
        else{
            let storyboard = UIStoryboard(name: "MyCars", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "EditCar")
            self.show(linkingVC, sender: self)
        }
    }
    
}
