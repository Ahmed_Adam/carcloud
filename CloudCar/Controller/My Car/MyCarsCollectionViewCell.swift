//
//  MyCarsCollectionViewCell.swift
//  CloudCar
//
//  Created by Adam on 6/14/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class MyCarsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var carLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var fixButton: UIButton!
    @IBOutlet weak var modelYear: UILabel!
    @IBOutlet weak var series: UILabel!
    @IBOutlet weak var carLogo: UIImageView!
    @IBOutlet weak var carPhoto: UIImageView!
    @IBOutlet weak var cat: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(sender:)))
        swipe.direction = .left
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
        
    }
    
    @objc func handleSwipe (sender: UISwipeGestureRecognizer) {
        carLogoConstraint.constant = 30
        editButton.isHidden = false
        deleteButton.isHidden = false
        fixButton.isHidden = false
        modelYear.isHidden = true
        series.isHidden = true
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleRightSwipe(sender:)))
        swipe.direction = .right
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
    }
    
    @objc func handleRightSwipe (sender: UISwipeGestureRecognizer) {
        carLogoConstraint.constant = 140
        editButton.isHidden = true
        deleteButton.isHidden = true
        fixButton.isHidden = true
        modelYear.isHidden = false
        series.isHidden = false
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(sender:)))
        swipe.direction = .left
        swipe.numberOfTouchesRequired = 1
        self.addGestureRecognizer(swipe)
    }

}
