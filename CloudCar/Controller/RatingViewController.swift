//
//  RatingViewController.swift
//  PopupDialog
//
//  Created by Martin Wildfeuer on 11.07.16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {

//    @IBOutlet weak var cosmosStarRating: CosmosView!

 //   @IBOutlet weak var commentTextField: UITextField!

    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var option1: UIButton!
    @IBOutlet weak var checkOption1: UIButton!
    @IBOutlet weak var option2: UIButton!
    @IBOutlet weak var checkOption2: UIButton!
    @IBOutlet weak var option3: UIButton!
    @IBOutlet weak var checkOption3: UIButton!
    @IBOutlet weak var label4: UIButton!
    @IBOutlet weak var option4: UIButton!
    @IBOutlet weak var label5: UIButton!
    @IBOutlet weak var option5: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let serviceName = SubService.getServiceName()
        self.serviceName.text = serviceName
        getSubCat()
        option1.isHidden = true
        checkOption1.isHidden = true
        option2.isHidden = true
        checkOption2.isHidden = true
        option3.isHidden = true
        checkOption3.isHidden = true
        
        option4.isHidden = true
        label4.isHidden = true
        option5.isHidden = true
        label5.isHidden = true

  //      commentTextField.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }
    
    func getSubCat(){
//        let id = SubService.getSubServiceID()
//        ServicesAPI.sharedInstance.GetSubCategories(catID: id!) { (error, secess, subCat) in
//            if subCat.data.count != 0 {
//                if subCat.data.count == 1 {
//                    self.option1.isHidden = false
//                    self.checkOption1.isHidden = false
//                    
//                    self.option1.setTitle(subCat.data[0].name, for: .normal)
//                }
//                if subCat.data.count == 2 {
//                    self.option1.isHidden = false
//                    self.checkOption1.isHidden = false
//                    self.option2.isHidden = false
//                    self.checkOption2.isHidden = false
//                    self.option1.setTitle(subCat.data[0].name, for: .normal)
//                    self.option2.setTitle(subCat.data[1].name, for: .normal)
//                }
//                if subCat.data.count == 3 {
//                    self.option1.setTitle(subCat.data[0].name, for: .normal)
//                    self.option2.setTitle(subCat.data[1].name, for: .normal)
//                    self.option3.setTitle(subCat.data[2].name, for: .normal)
//                }
//            }
//            
//        }
    }


    @IBAction func option(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option2(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option3(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option4(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option5(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    
    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension RatingViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
