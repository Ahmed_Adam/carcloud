//
//  ActivationCodeViewController.swift
//  CloudCar
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivationCodeViewController: UIViewController , NVActivityIndicatorViewable , UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBOutlet weak var codeText: UITextField!
    @IBAction func Verify(_ sender: UIButton) {
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Verifing ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
        }
        verify()
    }
    func verify(){
        let info = ["code": codeText.text!]
        UserAPIs.sharedInstance.ActivationCode(info: info) { (error , success , response ) in
            if response.code == 200 && response.data.active_code == 1 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
                self.present(linkingVC, animated: true, completion: nil)
            }
           
            else {
      
                let alert = UIAlertController(title: "Alert", message: "incorrect code" , preferredStyle: UIAlertControllerStyle.alert)

                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                    case .cancel:
                        print("cancel")

                    case .destructive:
                        print("destructive")


                    }}))
            }
            
        }
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
