//
//  ViewController.swift
//  CloudCar
//
//  Created by mac on 5/31/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class LogViewController: UIViewController , NVActivityIndicatorViewable{
    
    @IBOutlet weak var ababic: UIButton!
    override func viewDidLoad() {
        
    }
    
    @IBAction func EnglishClicked(_ sender: UIButton) {
        
        CheckLanguage.saveCheckLanguage(languge: "en")
        MOLH.setLanguageTo("en")
        MOLH.reset()
    }
    
    @IBAction func ArabicClicked(_ sender: UIButton) {
        CheckLanguage.saveCheckLanguage(languge: "ar")
        MOLH.setLanguageTo("ar")
        MOLH.reset()
    }
}

