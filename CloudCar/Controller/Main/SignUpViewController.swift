//
//  SignUpViewController.swift
//  CloudCar
//
//  Created by Adam on 6/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import CountryPicker
import NVActivityIndicatorView



class SignUpViewController: UIViewController , UITextFieldDelegate , CountryPickerDelegate , NVActivityIndicatorViewable {

    
    var  picker = CountryPicker()

    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var flag: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryImage: UIButton!
    @IBOutlet weak var middleView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        middleView.layer.borderWidth = 1
        middleView.layer.borderColor = UIColor.lightGray.cgColor
        middleView.dropShadow()
        
        phoneTextField.isEnabled = true
        phoneTextField.inputView?.isHidden = false
        
        codeTextField.inputView = picker
        flag.inputView = picker
        picker.countryPickerDelegate = self
       
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.codeTextField.text = phoneCode
        self.countryImage.setImage(flag, for: .normal)
        
    }

    @IBAction func signUp(_ sender: UIButton) {
        signup()
        
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Authenticating...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
        }
    
    }
    
    
    func signup(){
        let info = ["telephone": codeTextField.text! + phoneTextField.text!,
            "password":password.text!,
            "remember_token":"",
            "device_id":"",
            "name":name.text!,
            "email":email.text!]
        
        UserAPIs.sharedInstance.register(info: info) { (error, success , response ) in

            if response.code == 200  {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Verify")
            self.show(linkingVC, sender: self)
            }
            
            else {
                var msg1 = ""
                var msg2 = ""
                var msg3 = ""
                if response.msg.email.count != 0 {
                     msg1 = response.msg.email[0]
                }
                if response.msg.telephone.count != 0 {
                     msg2 = response.msg.telephone[0]
                }
                if response.msg.password.count != 0 {
                    msg3 = response.msg.password[0]
                }
                
                let alert = UIAlertController(title: "Alert", message: "\(msg1)  \n \(msg2) \n \(msg3)" , preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
            }

        }
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
