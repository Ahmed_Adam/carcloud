//
//  SignInViewController.swift
//  CloudCar
//
//  Created by Adam on 6/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import CountryPicker
import NVActivityIndicatorView

class SignInViewController: UIViewController , CountryPickerDelegate , UITextFieldDelegate , NVActivityIndicatorViewable{
    
    
    var delegate : stringToSendDelegate? = nil
    @IBOutlet weak var codeTextField: UITextField!
    var  picker = CountryPicker()
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var middleView: UIView!
        @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryImage: UIButton!
    @IBOutlet weak var flag: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        middleView.layer.borderWidth = 1
        middleView.layer.borderColor = UIColor.lightGray.cgColor
        middleView.dropShadow()
        phoneTextField.isEnabled = true
        phoneTextField.inputView?.isHidden = false
        
        codeTextField.inputView = picker
        flag.inputView = picker
        picker.countryPickerDelegate = self
        password.isEnabled = true
        
        self.password.superview!.bringSubview(toFront: self.password)
        
    }

    @IBAction func Login(_ sender: UIButton) {
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Authenticating...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
        }
        login()
    
    }
    
    
    func login(){
        
        let info = ["telephone": codeTextField.text! + phoneTextField.text! ,
                    "password": password.text! ,
                    "device_id": ""] as [String : String]
        UserAPIs.sharedInstance.login(info: info ) { (error, success , response) in
            if response.code == 200 {
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
                self.present(linkingVC, animated: true, completion: nil)
            }
            
            else {
                var msg1 = ""
                var msg2 = ""
                var msg3 = ""
                var msg4 = ""
                if response.msg.email.count != 0 {
                    msg1 = response.msg.email[0]
                }
                if response.msg.telephone.count != 0 {
                    msg2 = response.msg.telephone[0]
                }
                if response.msg.password.count != 0 {
                    msg3 = response.msg.password[0]
                }
                if response.msg.phone.count != 0 {
                    msg4 = response.msg.phone[0]
                }
                
                let alert = UIAlertController(title: "Alert", message: "\(msg1)  \n \(msg2) \n \(msg3) \n \(msg4) " , preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
            }
            
        }
    }

    @IBAction func FBLogin(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    
                }
            }
        }
    }
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                 let   accessToken = FBSDKAccessToken.current().tokenString
                    //everything works print the user data
                    print(result!,accessToken!)
                }
            })
        }
    }
    
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.codeTextField.text = phoneCode
        self.countryImage.setImage(flag, for: .normal)

        
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        layer.shadowRadius = 5
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}



