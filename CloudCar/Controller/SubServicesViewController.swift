//
//  SubServicesViewController.swift
//  CloudCar
//
//  Created by mac on 7/9/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class SubServicesViewController: UIViewController {

    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var stackOptions: UIStackView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var option1: UIButton!
    @IBOutlet weak var checkOption1: UIButton!
    @IBOutlet weak var option2: UIButton!
    @IBOutlet weak var checkOption2: UIButton!
    @IBOutlet weak var option3: UIButton!
    @IBOutlet weak var checkOption3: UIButton!
    @IBOutlet weak var label4: UIButton!
    @IBOutlet weak var option4: UIButton!
    @IBOutlet weak var label5: UIButton!
    @IBOutlet weak var option5: UIButton!
    @IBOutlet weak var label: UITextView!
    @IBOutlet weak var subAvalable: UILabel!
    
    var subCat: SubCatRootModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        option1.isHidden = true
        checkOption1.isHidden = true
        option2.isHidden = true
        checkOption2.isHidden = true
        option3.isHidden = true
        checkOption3.isHidden = true
        option4.isHidden = true
        label4.isHidden = true
        option5.isHidden = true
        label5.isHidden = true
        let serviceName = SubService.getServiceName()
        self.serviceName.text = serviceName
        getSubCat()
                view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }


    func getSubCat(){
                let id = SubService.getSubServiceID()
                ServicesAPI.sharedInstance.GetSubCategories(catID: id!) { (error, secess, subCat) in
                    self.subCat = subCat
                    if subCat.data.count != 0 {
                        if subCat.data.count == 1 {
                            self.option1.isHidden = false
                            self.checkOption1.isHidden = false
        
                            self.option1.setTitle(subCat.data[0].name, for: .normal)
                        }
                        if subCat.data.count == 2 {
                            self.option1.isHidden = false
                            self.checkOption1.isHidden = false
                            self.option2.isHidden = false
                            self.checkOption2.isHidden = false
                            self.option1.setTitle(subCat.data[0].name, for: .normal)
                            self.option2.setTitle(subCat.data[1].name, for: .normal)
                        }
                        if subCat.data.count == 3 {
                            self.option1.setTitle(subCat.data[0].name, for: .normal)
                            self.option2.setTitle(subCat.data[1].name, for: .normal)
                            self.option3.setTitle(subCat.data[2].name, for: .normal)
                        }
                    }
                    else{
                        self.subAvalable.isHidden = false
                    }
        
                }
    }
    
    var subCatIDs = ""
    @IBAction func option(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            //self.subCat.data[0].name
           let id =  self.subCat.data[0].id
            subCatIDs.append("\(id)")
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            SubServices.saveSubServices(SubServices: subCatIDs)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option2(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            let id =  self.subCat.data[1].id
            subCatIDs.append(",\(id)")
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            SubServices.saveSubServices(SubServices: subCatIDs)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
            
        }
    }
    @IBAction func option3(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            let id =  self.subCat.data[2].id
            subCatIDs.append(",\(id)")
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            SubServices.saveSubServices(SubServices: subCatIDs)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option4(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            let id =  self.subCat.data[3].id
            subCatIDs.append(",\(id)")
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            SubServices.saveSubServices(SubServices: subCatIDs)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    @IBAction func option5(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            let id =  self.subCat.data[4].id
            subCatIDs.append(",\(id)")
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            SubServices.saveSubServices(SubServices: subCatIDs)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    
    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension SubServicesViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
