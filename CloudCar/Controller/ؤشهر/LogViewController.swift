//
//  ViewController.swift
//  CloudCar
//
//  Created by mac on 5/31/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class LogViewController: UIViewController {

    @IBOutlet weak var logInViewButton: UIButton!
    @IBOutlet weak var signUPButton: UIButton!
    @IBOutlet weak var loginview: UIView!
    @IBOutlet weak var signUPView: UIView!
    
    
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var haveaccStack: UIStackView!
    @IBOutlet weak var faceBookButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    
    @IBOutlet weak var regUserNameView: UIView!
    @IBOutlet weak var regEmailView: UIView!
    @IBOutlet weak var regPasswordView: UIView!
    @IBOutlet weak var regconfirmPasswordView: UIView!
    
    
    @IBOutlet weak var SignInViewContainer: UIView!
    @IBOutlet var SignUPViewContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       signUPView.isHidden = true
        SignUPViewContainer.isHidden = true
        userNameView.layer.borderWidth = 2
        userNameView.layer.borderColor = UIColor.lightGray.cgColor
        passwordView.layer.borderWidth = 2
        passwordView.layer.borderColor = UIColor.lightGray.cgColor
        
        regUserNameView.layer.borderWidth = 2
        regUserNameView.layer.borderColor = UIColor.lightGray.cgColor
        regEmailView.layer.borderWidth = 2
        regEmailView.layer.borderColor = UIColor.lightGray.cgColor
        regPasswordView.layer.borderWidth = 2
        regPasswordView.layer.borderColor = UIColor.lightGray.cgColor
        regconfirmPasswordView.layer.borderWidth = 2
        regconfirmPasswordView.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    @IBAction func SignUpButton(_ sender: UIButton) {
        signUPView.isHidden = false
        loginview.isHidden = true
        signUPButton.setTitleColor(UIColor.orange, for: .normal)
        logInViewButton.setTitleColor(UIColor.lightGray, for: .normal)
        SignUPViewContainer.isHidden = false
      
        
        
    }
    
    @IBAction func LogIn(_ sender: UIButton) {
        self.SignUPViewContainer.isHidden = true
       
        signUPView.isHidden = true
        loginview.isHidden = false
        SignUPViewContainer.isHidden = true
        logInViewButton.setTitleColor(UIColor.orange, for: .normal)
        signUPButton.setTitleColor(UIColor.lightGray, for: .normal)
        
    }
    

}

extension LogViewController: UITextFieldDelegate {
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
}

