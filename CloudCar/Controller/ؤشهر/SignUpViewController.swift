//
//  SignUpViewController.swift
//  CloudCar
//
//  Created by Adam on 6/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import CountryPicker



class SignUpViewController: UIViewController , UITextFieldDelegate , CountryPickerDelegate  {

    
    var  picker = CountryPicker()
    
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var flag: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryImage: UIButton!
    @IBOutlet weak var middleView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        middleView.layer.borderWidth = 1
        middleView.layer.borderColor = UIColor.lightGray.cgColor
        middleView.dropShadow()
        
        phoneTextField.isEnabled = true
        phoneTextField.inputView?.isHidden = false
        
        codeTextField.inputView = picker
        flag.inputView = picker
        picker.countryPickerDelegate = self
       
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.codeTextField.text = phoneCode
        self.countryImage.setImage(flag, for: .normal)
        
    }

    @IBAction func signUp(_ sender: UIButton) {
        signup()
    }
    
    
    func signup(){
        let info = ["telephone": codeTextField.text! + phoneTextField.text!,
            "password":password.text!,
            "remember_token":"",
            "device_id":"",
            "name":name.text!,
            "email":email.text!]
        
        UserAPIs.sharedInstance.register(info: info) { (error, success , response ) in
            
         //////////////////////////////
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Verify")
            self.show(linkingVC, sender: self)
            
        }
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
