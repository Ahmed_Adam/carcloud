//
//  MyCarHomeViewController.swift
//  CloudCar
//
//  Created by mac on 6/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import DatePickerDialog

class MyCarHomeViewController: UIViewController {
  
        @IBOutlet weak var brandView: UIView!

        @IBOutlet weak var carNumber: UIView!
        @IBOutlet weak var locationView: UIView!
        @IBOutlet weak var yearManufature: UIView!
        
        @IBOutlet weak var dayTextField_f: UITextField!
        @IBOutlet weak var monthTextField_f: UITextField!
        @IBOutlet weak var dayNamae_f: UITextField!
        @IBOutlet weak var timeTextFiled_f: UITextField!
        @IBOutlet weak var yearTextFiled_f: UITextField!
        override func viewDidLoad() {
            super.viewDidLoad()
            setBorders()
            brandView.dropShadow()
            carNumber.dropShadow()

            dayTextField_f.delegate = self
            monthTextField_f.delegate = self
            dayNamae_f.delegate = self
            timeTextFiled_f.delegate = self
            yearTextFiled_f.delegate = self
        }
        
        func setBorders(){
            brandView.layer.borderWidth = 1
            brandView.layer.borderColor = UIColor.lightGray.cgColor

            yearManufature.layer.borderWidth = 1
            yearManufature.layer.borderColor = UIColor.lightGray.cgColor
            locationView.layer.borderWidth = 1
            locationView.layer.borderColor = UIColor.lightGray.cgColor

            carNumber.layer.borderWidth = 1
            carNumber.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        @IBAction func ChooseDateButton(_ sender: UIButton) {
            datePickerTapped()
        }
        
        @IBAction func ChooseTimeButton(_ sender: UIButton) {
            timePickerTapped()
        }
        
        
        func datePickerTapped() {
            
            
            let datePicker = DatePickerDialog(textColor: .black,
                                              buttonColor: .black,
                                              font: UIFont.boldSystemFont(ofSize: 17),
                                              showCancelButton: true)
            datePicker.show("Choose a date",
                            doneButtonTitle: "Done",
                            cancelButtonTitle: "Cancel",
                            datePickerMode: .date) { (date) in
                                if let dt = date {
                                    let formatter = DateFormatter()
                                    //   formatter.dateFormat = "EEEE, MMM d, yyyy"
                                    formatter.dateFormat = "d"
                                    self.dayTextField_f.text = formatter.string(from: dt)
                                    formatter.dateFormat = "MMMM"
                                    self.monthTextField_f.text = formatter.string(from: dt)
                                    formatter.dateFormat = "EEEE"
                                    self.dayNamae_f.text = formatter.string(from: dt)
                                    formatter.dateFormat = "yyyy"
                                    self.yearTextFiled_f.text = formatter.string(from: dt)
                                    //                                formatter.dateFormat = "h:mm a"
                                    //                                self.timeTextFiled_f.text = formatter.string(from: dt)
                                    
                                    
                                }
            }
        }
        
        func timePickerTapped() {
            
            
            let datePicker = DatePickerDialog(textColor: .black,
                                              buttonColor: .black,
                                              font: UIFont.boldSystemFont(ofSize: 17),
                                              showCancelButton: true)
            datePicker.show("Choose a time ",
                            doneButtonTitle: "Done",
                            cancelButtonTitle: "Cancel",
                            datePickerMode: .time) { (time) in
                                if let dt = time {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "h:mm a"
                                    self.timeTextFiled_f.text = formatter.string(from: dt)
                                    
                                    
                                }
            }
        }
        @IBAction func onMoreTapped() {
            print("TOGGLE SIDE MENU")
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
            
        }
        
    }
    
    extension MyCarHomeViewController: UITextFieldDelegate {
        
        /**
         * Called when 'return' key pressed. return NO to ignore.
         */
        private func textFieldShouldReturn(textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        
        /**
         * Called when the user click on the view (outside the UITextField).
         */
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            if textField == self.dayTextField_f || textField == self.monthTextField_f || textField == self.dayNamae_f || textField == self.yearTextFiled_f
            {
                datePickerTapped()
                return false
            }
            else  if textField == self.timeTextFiled_f
            {
                timePickerTapped()
                return false
            }
            
            return true
        }
}


