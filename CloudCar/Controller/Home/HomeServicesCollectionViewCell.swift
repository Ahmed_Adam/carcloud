//
//  HomeServicesCollectionViewCell.swift
//  CloudCar
//
//  Created by mac on 6/3/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class HomeServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var price: UILabel!
    
    
}
