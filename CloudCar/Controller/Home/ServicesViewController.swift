//
//  ServicesViewController.swift
//  CloudCar
//
//  Created by Adam on 6/14/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import PopupDialog

class ServicesViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource  , UICollectionViewDelegateFlowLayout {
    var clicked = false
    var services : ServicesRoot!
    override func viewDidLoad() {
        super.viewDidLoad()
        SubServices.saveSubServices(SubServices: "")
//        self.collectionView.legate = self
//        self.collectionView.dataSource = self
        getServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getServices()
    }
    
    func getServices(){
        let info = ["asd":"",
            "as":"",
            "ad":""
        ]
        ServicesAPI.sharedInstance.getServices(info: info , complition: { (error , success , services ) in
            self.services = services
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        })
    }

    @IBOutlet weak var collectionView: UICollectionView!
    func collectionView(_ catCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.services.data.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat((collectionView.frame.size.width - 30) / 2) , height: CGFloat(150))
    }
    
    
    
    
    func collectionView(_ catCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =   collectionView.dequeueReusableCell(withReuseIdentifier: "servicesCell", for: indexPath) as! servicesCollectionViewCell
        
//        cell.check.tag = indexPath.row
//        cell.check.superview?.tag = indexPath.section
//        cell.check.addTarget(self, action: #selector(check(_:)), for: .touchUpInside)
//
//        cell.service.image =
//        cell.serviceName.text = self.services.data[indexPath.row].name

        return cell
    }
    var select = false

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let cell = collectionView.cellForItem(at: indexPath) as! servicesCollectionViewCell

        SubService.saveSubServiceID(servicceID: self.services.data[indexPath.row].id)
        SubService.saveServiceName(servicceName: self.services.data[indexPath.row].name)
        
        self.select = !self.select
        if self.select{
            
            cell.check.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            cell.service.image = #imageLiteral(resourceName: "15")
        //    getSubCat(catID : self.cats.data[indexPath.row].id)
             showCustomDialog()
        }
        else{
            
            cell.check.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
            cell.service.image = #imageLiteral(resourceName: "gas_station_filled")
            cell.isSelected = true
        }
       
    }
    @IBAction func continuToWorkshop(_ sender: UIButton) {
        
        
        FromSideMenu.saveFromSideMenu(check: false )
        let storyboard = UIStoryboard(name: "Garage", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Garage")
        self.show(linkingVC, sender: self)
    }
    
    
    func showCustomDialog(animated: Bool = true) {
        
        
        
        // Create a custom view controller
        let ratingVC = SubServicesViewController(nibName: "SubServiceTableViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
   //         self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "Continue", height: 60) {

        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    @objc func check(_ sender: UIButton) {
        
 
 
    }

}
