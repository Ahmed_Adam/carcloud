//
//  HomeViewController.swift
//  CloudCar
//
//  Created by Adam on 6/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import DatePickerDialog
import NVActivityIndicatorView


class HomeViewController: UIViewController , NVActivityIndicatorViewable  {
    
    
    
    
    
    @IBOutlet weak var brandView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var yearManufature: UIView!
    @IBOutlet weak var chooseCarFirst: UILabel!
    @IBOutlet weak var carBrandType: UIImageView!
    @IBOutlet weak var carPhoto: UIButton!
    @IBOutlet weak var dayTextField_f: UITextField!
    @IBOutlet weak var monthTextField_f: UITextField!
    @IBOutlet weak var dayNamae_f: UITextField!
    @IBOutlet weak var timeTextFiled_f: UITextField!
    @IBOutlet weak var yearTextFiled_f: UITextField!
    @IBOutlet weak var addressName: UIButton!
    @IBOutlet weak var addressDetails: UIButton!
    @IBOutlet weak var carBrand: UILabel!
    
    
    var firstTime = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorders()
        brandView.dropShadow()
        dateView.dropShadow()
        dayTextField_f.delegate = self
        monthTextField_f.delegate = self
        dayNamae_f.delegate = self
        timeTextFiled_f.delegate = self
        yearTextFiled_f.delegate = self
        MyCar.setImage(image: #imageLiteral(resourceName: "ORANGE LOGO"))
        firstTime = 0
        Address.saveAddress(address: "Pick location ")
        Address.saveAddressName(name: "Address")
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        FromSideMenu.saveFromSideMenu(check: true)
        
        firstTime += 1
        let address =  Address.getAddress()
        let addressName = Address.getAddressName()
        let brand = MyCar.getCarModel()
        let brandOmage = MyCar.getCatImage(key: "catImage")
        if brand != nil || brandOmage != nil {
            chooseCarFirst.isHidden = true
            self.carBrandType.image = brandOmage
            self.carBrand.text = brand
        }
        if firstTime == 0{
            
        }
        getCar()
        if firstTime >= 2 {
            chooseCarFirst.isHidden = true
            if address != nil || addressName != nil || address != "Pick location " || addressName != "Address"{
                self.addressName.setTitleColor(UIColor.lightGray, for: .normal)
                self.addressDetails.setTitleColor(UIColor.lightGray, for: .normal)
                self.addressName.setTitle(addressName, for: .normal)
                self.addressDetails.setTitle(address, for: .normal)
            }
        }
    }
    var image : UIImage!
    func getCar (){
       
        self.image = MyCar.getCatImage(key: "catImage")
        carBrandType.image = image
        let def = UserDefaults.standard
        if let user_token = def.object(forKey: "token"){
            print (user_token)
            
 
        }
        else {

        }
    }
    
    func setBorders(){
        brandView.layer.borderWidth = 1
        brandView.layer.borderColor = UIColor.lightGray.cgColor
        yearManufature.layer.borderWidth = 1
        yearManufature.layer.borderColor = UIColor.lightGray.cgColor
        locationView.layer.borderWidth = 1
        locationView.layer.borderColor = UIColor.lightGray.cgColor
        dateView.layer.borderWidth = 1
        dateView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
    @IBAction func changeCar(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "MyCars", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "EditCar")
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage(" Getting Data ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            self.stopAnimating()
        }
        self.show(linkingVC, sender: self)
        // EditCar
    }
    
    
    
    @IBAction func MyCars(_ sender: UIButton) {
        FromSideMenu.saveFromSideMenu(check: false)
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage(" Getting Data ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            self.stopAnimating()
        }
        let storyboard = UIStoryboard(name: "MyCars", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "MyCars")
        self.show(linkingVC, sender: self)
        
    }
    
    @IBAction func pickUpCar(_ sender: UIButton) {
        FromSideMenu.saveFromSideMenu(check: false)
//        if addressDetails.currentTitle == "Address" {
//            let alert = UIAlertController(title: "Alert", message: "Choose Car first", preferredStyle: UIAlertControllerStyle.alert)
//
//            self.present(alert, animated: true, completion: nil)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//                switch action.style{
//                case .default:
//                    self.addressName.setTitleColor(UIColor.red, for: .normal)
//                    self.addressDetails.setTitleColor(UIColor.red, for: .normal)
//                    print("default")
//                case .cancel:
//                    print("cancel")
//
//                case .destructive:
//                    print("destructive")
//
//
//                }}))
//        }
//         if  chooseCarFirst.isEnabled {
//            let alert = UIAlertController(title: "Alert", message: "Please Choose a car ", preferredStyle: UIAlertControllerStyle.alert)
//
//            self.present(alert, animated: true, completion: nil)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//                switch action.style{
//                case .default:
//                    self.chooseCarFirst.textColor = UIColor.red
//                    print("default")
//                case .cancel:
//                    print("cancel")
//
//                case .destructive:
//                    print("destructive")
//
//
//                }}))
//        }
//        else {
            let storyboard = UIStoryboard(name: "Garage", bundle: nil)
            let linkingVC = storyboard.instantiateViewController(withIdentifier: "Garage")
        
            let size = CGSize(width: 30, height: 30)
            
            startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballBeat)
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                NVActivityIndicatorPresenter.sharedInstance.setMessage(" Getting Data ...")
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                self.stopAnimating()
            }
            self.show(linkingVC, sender: self)
        //}
        
    }
    

    func datePickerTapped() {
        
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Choose a date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        datePickerMode: .date) { (date) in
                            if let dt = date {
                                let formatter = DateFormatter()
                                let timeInterval = datePicker.datePicker.date.timeIntervalSince1970
                                let unixTime = NSDate(timeIntervalSince1970: timeInterval)
                                
                                print(unixTime)
                                //   formatter.dateFormat = "EEEE, MMM d, yyyy"
                                formatter.dateFormat = "d"
                                self.dayTextField_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "MMMM"
                                self.monthTextField_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "EEEE"
                                self.dayNamae_f.text = formatter.string(from: dt)
                                formatter.dateFormat = "yyyy"
                                self.yearTextFiled_f.text = formatter.string(from: dt)
                                //                                formatter.dateFormat = "h:mm a"
                                //                                self.timeTextFiled_f.text = formatter.string(from: dt)
                                
                                
                            }
        }
    }
    
    func timePickerTapped() {
        
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Choose a time ",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        datePickerMode: .time) { (time) in
                            if let dt = time {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "h:mm a"
                                self.timeTextFiled_f.text = formatter.string(from: dt)
                                
                                
                            }
        }
    }
}

extension HomeViewController: UITextFieldDelegate {
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.dayTextField_f || textField == self.monthTextField_f || textField == self.dayNamae_f || textField == self.yearTextFiled_f
        {
            datePickerTapped()
            return false
        }
        else  if textField == self.timeTextFiled_f
        {
            timePickerTapped()
            return false
        }
        
        return true
    }
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }
    
}



