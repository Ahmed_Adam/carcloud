//
//  servicesCollectionViewCell.swift
//  CloudCar
//
//  Created by Adam on 6/14/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class servicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var conainerView: UIView!
    @IBOutlet weak var service: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var check: UIButton!
    override func awakeFromNib() {
        conainerView.layer.borderWidth = 1
        conainerView.layer.borderColor = UIColor.lightGray.cgColor
        conainerView.dropShadow()
    }
}
