//
//  CarLocationViewController.swift
//  CloudCar
//
//  Created by mac on 6/26/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import GoogleMaps



class CarLocationViewController: UIViewController , CLLocationManagerDelegate , GMSAutocompleteFetcherDelegate {
    
    
    

    let vc:HomeViewController! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
    
        placesClient = GMSPlacesClient.shared()
    }

    var placesClient: GMSPlacesClient!
     @IBOutlet var googleMapsContainerView: UIView!
    var resultsArray = [String]()
    var googleMapsView:GMSMapView!
    var gmsFetcher: GMSAutocompleteFetcher!
    var locationManager = CLLocationManager()
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        self.googleMapsView = GMSMapView (frame: self.googleMapsContainerView.frame)
        self.googleMapsView.settings.compassButton = true
        self.googleMapsView.isMyLocationEnabled = true
        self.googleMapsView.settings.myLocationButton = true
        self.view.addSubview(self.googleMapsView)
       
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
           getCurrentLocation()
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation = locations.last
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 15);
        self.googleMapsView.camera = camera
        self.googleMapsView.isMyLocationEnabled = true
        
        let marker = GMSMarker(position: center)
        Location.saveLat(Lat:"\(userLocation!.coordinate.latitude)")
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        Location.saveLong(Long: "\(userLocation!.coordinate.longitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
        marker.map = self.googleMapsView
        
        marker.title = "Current Location"
        locationManager.stopUpdatingLocation()
    }
    // Add a pair of UILabels in Interface Builder, and connect the outlets to these variables.
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!

    // Add a UIButton in Interface Builder, and connect the action to this function.
    @IBAction func getCurrentPlace(_ sender: UIButton) {
       getCurrentLocation()

    }
    
    func getCurrentLocation(){
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                
            }
            
            self.nameLabel.text = "No current place"
            self.addressLabel.text = ""
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    self.nameLabel.text = place.name
                    self.addressLabel.text = place.formattedAddress
                    let address = place.formattedAddress
                    Address.saveAddress(address: address!)
                    Address.saveAddressName(name: place.name)
                    
                    
                    
                }
            }
        })
    }
    
    // Add a UIButton in Interface Builder, and connect the action to this function.
    @IBAction func pickPlace(_ sender: UIButton) {
        let center = CLLocationCoordinate2D(latitude: 37.788204, longitude: -122.411937)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                self.nameLabel.text = place.name
                self.addressLabel.text = place.formattedAddress?.components(separatedBy: ", ")
                    .joined(separator: "\n")
            } else {
                self.nameLabel.text = "No place selected"
                self.addressLabel.text = ""
            }
        })
    }
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        
    }
    
}
