////
////  HomeViewController.swift
////  CloudCar
////
////  Created by mac on 5/31/18.
////  Copyright © 2018 EasyDevs. All rights reserved.
////
//
//import UIKit
//import DatePickerDialog
//import DropDown
//
//
//class OldHomeViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
//
//    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var dayTextField_f: UITextField!
//    @IBOutlet weak var monthTextField_f: UITextField!
//    @IBOutlet weak var dayNamae_f: UITextField!
//    @IBOutlet weak var timeTextFiled_f: UITextField!
//        @IBOutlet weak var yearTextFiled_f: UITextField!
//    
//    let logos = [#imageLiteral(resourceName: "Air Condition") ,#imageLiteral(resourceName: "Electricity") , #imageLiteral(resourceName: "Gearbox") , #imageLiteral(resourceName: "General"), #imageLiteral(resourceName: "Machine") ,#imageLiteral(resourceName: "Metal Work")  ]
//    let Names = ["Air Conditionaing","Electricity" , "GearBox" , "General","Machine" ,"Metal Works" ]
//    let cars = ["PMW","Marcides","Opel","Toyota","Kea"]
//    let series = ["series 1","series 2","series 3","series 4","series 5"]
//    let year = ["2016","2017","2018","2019","2020"]
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        
//        dayTextField_f.delegate = self
//        monthTextField_f.delegate = self
//        dayNamae_f.delegate = self
//        timeTextFiled_f.delegate = self
//    }
//    
// 
//    @IBAction func ChooseDateButton(_ sender: UIButton) {
//      datePickerTapped()
//    }
//    
//    @IBAction func ChooseTimeButton(_ sender: UIButton) {
//        timePickerTapped()
//    }
//    
//      @objc func check(_ sender: UIButton) {
//        
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected{
//        
//        sender.setImage(#imageLiteral(resourceName: "check_filled"), for: .normal)
//        }
//        else{
//                sender.setImage(nil, for: .normal)
//            }
//        
////        sender.backgroundColor = UIColor.white
////        sender.frame.size.width = 30
////        sender.frame.size.height = 30
//        
//        
//    }
//    
//    
//    @IBAction func selectCar(_ sender: UIButton) {
//        
//        
//        let dd = DropDown()
//        dd.anchorView = (sender as AnchorView)
//        dd.dataSource = cars
//        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
//        dd.selectionAction = {(index : Int , item : String)in
//            sender.setTitle(item, for: .normal)
//           
//            
//        }
//        dd.show()
//        
//    }
//    
//    @IBAction func selectmodel(_ sender: UIButton) {
//        
//        
//        let dd = DropDown()
//        dd.anchorView = (sender as AnchorView)
//        dd.dataSource = series
//        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
//        dd.selectionAction = {(index : Int , item : String)in
//            sender.setTitle(item, for: .normal)
//            
//            
//        }
//        dd.show()
//        
//    }
//    
//    @IBAction func selectyear(_ sender: UIButton) {
//        
//        
//        let dd = DropDown()
//        dd.anchorView = (sender as AnchorView)
//        dd.dataSource = year
//        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
//        dd.selectionAction = {(index : Int , item : String)in
//            sender.setTitle(item, for: .normal)
//            
//            
//        }
//        dd.show()
//        
//    }
//    
//    
//    func datePickerTapped() {
//        
//        
//        let datePicker = DatePickerDialog(textColor: .black,
//                                          buttonColor: .black,
//                                          font: UIFont.boldSystemFont(ofSize: 17),
//                                          showCancelButton: true)
//        datePicker.show("Choose a date",
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        datePickerMode: .date) { (date) in
//                            if let dt = date {
//                                let formatter = DateFormatter()
//                                //   formatter.dateFormat = "EEEE, MMM d, yyyy"
//                                formatter.dateFormat = "d"
//                                self.dayTextField_f.text = formatter.string(from: dt)
//                                formatter.dateFormat = "MMMM"
//                                self.monthTextField_f.text = formatter.string(from: dt)
//                                formatter.dateFormat = "EEEE"
//                                self.dayNamae_f.text = formatter.string(from: dt)
//                                formatter.dateFormat = "yyyy"
//                                self.yearTextFiled_f.text = formatter.string(from: dt)
////                                formatter.dateFormat = "h:mm a"
////                                self.timeTextFiled_f.text = formatter.string(from: dt)
//                                
//                                
//                            }
//        }
//    }
//    
//    func timePickerTapped() {
//        
//        
//        let datePicker = DatePickerDialog(textColor: .black,
//                                          buttonColor: .black,
//                                          font: UIFont.boldSystemFont(ofSize: 17),
//                                          showCancelButton: true)
//        datePicker.show("Choose a time ",
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        datePickerMode: .time) { (time) in
//                            if let dt = time {
//                                let formatter = DateFormatter()
//                                  formatter.dateFormat = "h:mm a"
//                                  self.timeTextFiled_f.text = formatter.string(from: dt)
//                                
//                                
//                            }
//        }
//    }
//    
//
//
//    
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 5
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath)as! HomeServicesCollectionViewCell
//        
//         cell.photo.image = self.logos[indexPath.row]
//        cell.serviceName.text = self.Names[indexPath.row]
//        cell.checkButton.tag = indexPath.row
//        cell.checkButton.superview?.tag = indexPath.section
//        cell.checkButton.addTarget(self, action: #selector(check(_:)), for: .touchUpInside)
//        return cell
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        return CGSize(width: CGFloat(((collectionView.frame.size.width - 20 ) / 2) ), height: CGFloat(160))
//    }
//
//}
//
//
//
//extension HomeViewController: UITextFieldDelegate {
//    
//    /**
//     * Called when 'return' key pressed. return NO to ignore.
//     */
//    private func textFieldShouldReturn(textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//    
//    
//    /**
//     * Called when the user click on the view (outside the UITextField).
//     */
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
//    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == self.dayTextField_f || textField == self.monthTextField_f || textField == self.dayNamae_f
//        {
//            datePickerTapped()
//            return false
//        }
//        else  if textField == self.timeTextFiled_f
//        {
//            timePickerTapped()
//            return false
//        }
//        
//        return true
//    }
//}
