//
//  InvoiceViewController.swift
//  CloudCar
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class InvoiceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       // dropShadow()
        setBorders(view: spead)
        setBorders(view: view2)
        setBorders(view: view3)
        setBorders(view: abc)
        backButton.isHidden = true
    }
    @IBOutlet weak var backButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        let chck = FromSideMenu.getFromSideMenu()
        if chck != false {
            backButton.isHidden = false
        }
    }
    
    
    
    @IBOutlet weak var spead: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var abc: UIView!
    func dropShadow(){
        spead.dropShadow()
        spead.dropShadow()
        view2.dropShadow()
        view3.dropShadow()
        abc.dropShadow()
    }
    func setBorders(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
    }

    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            let activity = self.storyboard?.instantiateViewController(withIdentifier: "Activities")
            self.show(activity!, sender: self)
        }
    }
    
}
