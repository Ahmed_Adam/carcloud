//
//  workshopCollectionViewCell.swift
//  CloudCar
//
//  Created by mac on 7/9/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class workshopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var workshopImage: UIImageView!
    @IBOutlet weak var workshopName: UILabel!
    @IBOutlet weak var workshopAddress: UILabel!
    @IBOutlet weak var viewBorder: UIView!
    
    
}
