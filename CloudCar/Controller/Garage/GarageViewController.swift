//
//  GarageViewController.swift
//  CloudCar
//
//  Created by mac on 6/18/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import PopupDialog

class GarageViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout ,UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = paymentTableview.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        paymentView.isHidden = true
        paymentButton.setTitle("payment method 1 ", for: .normal)
    }
    
    
    @IBOutlet weak var paymentButton: UIButton!
    let logos = [#imageLiteral(resourceName: "5") ,#imageLiteral(resourceName: "time") , #imageLiteral(resourceName: "3") , #imageLiteral(resourceName: "10"), #imageLiteral(resourceName: "17") ,#imageLiteral(resourceName: "gas_station_filled")  ]
    let Names = ["Air Conditionaing","Electricity" , "GearBox" , "General","Machine" ,"Metal Works" ]
    let cars = ["PMW","Marcides","Opel","Toyota","Kea"]
    let series = ["series 1","series 2","series 3","series 4","series 5"]
    let year = ["2016","2017","2018","2019","2020"]
  //  var workshops : WorkshopRoot!
    var workshop : WorkShopData!
    @IBOutlet weak var collectionView: UICollectionView!
        @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var winchLabel: UILabel!
    @IBOutlet weak var winchPrice: UILabel!
    @IBOutlet weak var winchLogo: UIImageView!
    @IBOutlet weak var locationVIew: UIView!
    @IBOutlet weak var city: UIButton!
    @IBOutlet weak var availableVIew: UIView!
        @IBOutlet weak var periodVIew: UIView!
          @IBOutlet weak var winshVIew: UIView!
    @IBOutlet weak var minServiceVIew: UIView!
    @IBOutlet weak var winchViewHieght: NSLayoutConstraint!
    @IBOutlet weak var paymentTableview: UITableView!
    
    @IBOutlet weak var comment: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        paymentTableview.delegate = self
        paymentTableview.dataSource = self
        dropShadow()
        setBorders(view: locationVIew)
        setBorders(view: availableVIew)
        setBorders(view: periodVIew)
        setBorders(view: winshVIew)
        setBorders(view: minServiceVIew)
        setBorders(view: payview)
        backButton.isHidden = true

        comment.layer.borderWidth = 1
        comment.layer.borderColor = UIColor.lightGray.cgColor
        
    }
   

    @IBOutlet weak var payview: UIView!
    

    
    @IBOutlet weak var availble: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        paymentView.isHidden = true
        self.availble.isHidden = true
        let chck = FromSideMenu.getFromSideMenu()
        if chck != false {
            backButton.isHidden = false
        }
        if workshop != nil {
            self.city.setTitle(workshop.city, for: .normal)
            if workshop.winch == "1" {
                yesWinch()
                
            }
            else {
                winchViewHieght.constant = 0
                
                noWinch()
            }
        }
        
        if workshop.SreviceCars.count == 0{
            self.availble.isHidden = false
        }
    }
    func noWinch(){
        winchViewHieght.constant = 0
        winshVIew.isHidden = true
        winchCheck.isHidden = true
        winchLabel.isHidden = true
        winchPrice.isHidden = true
        winchLogo.isHidden = true
    }
    func yesWinch(){
        
        winshVIew.isHidden = false
        winchCheck.isHidden = false
        winchLabel.isHidden = false
        winchPrice.isHidden = false
        winchLogo.isHidden = false
    }
    let loctionLat = Location.getLat()
    let locatLong = Location.getLong()
    var winchav = ""
    @IBOutlet weak var winchCheck: UIButton!
    @IBAction func winchCheck(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            if self.locatLong == "" || self.loctionLat == "" {
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            let alert = UIAlertController(title: "Alert", message: " please pick location " , preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.winchav = "1"
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
                    self.present(linkingVC, animated: true, completion: nil)
                case .cancel:
                    print("cancel")
                    self.winchav = "0"
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            }
            else {
                sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
        
    }
        @IBOutlet weak var BBtn: UIButton!
    var orderresponse : MyDatorahPayment!
    var baymentTypeID = ""
    @IBAction func storeOrder(_ sender: UIButton) {
        CheckCar ()
        if self.BBtn.currentTitle == "payment method 1 "{
            baymentTypeID = "1"
        }
        else {
            baymentTypeID = "2"
        }
        var subIds = SubServices.getSubServices()
        if subIds == nil {
            subIds = ""
        }

        let info = ["workShop_id": "\(self.workshop.id)",
                          "car_id"   :carid,
                          "price"   :"5555",
                          "payment_type_id"   :baymentTypeID,
                          "winch_date"   : winchav,
                          "lat"   :loctionLat!,
                          "long"   : locatLong! ,
                          "comment"   :comment.text!,
                          "service_id"   : subIds!,
                          "winch"   : workshop.winch
        ]
//        let info = ["workShop_id": "4",
//            "car_id"   :"22",
//            "price"   :"5555",
//            "payment_type_id"   :"2",
//            "winch_date"   : "1531386000",
//            "lat"   :"432545454",
//            "long"   : "432545454" ,
//            "comment"   :"432545454",
//            "service_id"   : "1,2,3",
//            "winch"   : "1"
//        ]
        
        
        UserOwnsAPIs.sharedInstance.storeOrder(info: info) { (error , success , order) in
            
            self.orderresponse = order
            if order.code == -1 {
                let alert = UIAlertController(title: "Alert", message: " Order falied " , preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                         DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                      
                        }
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
            }
            else if self.orderresponse.check == true && self.orderresponse.code == 200 && self.orderresponse.failedUrl == "" && self.orderresponse.successUrl == "" {
                
                let alert = UIAlertController(title: "Alert", message: self.orderresponse.status , preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    alert.dismiss(animated: true, completion: nil)
                    let storyboard = UIStoryboard(name: "Payment", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "Payment") as! PaymentURlsViewController
                    linkingVC.orderresponse = self.orderresponse
                    self.show(linkingVC, sender: self)
                }
            }
            else {
                let alert = UIAlertController(title: "Alert", message: self.orderresponse.status , preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    alert.dismiss(animated: true, completion: nil)
                    
                }
                let storyboard = UIStoryboard(name: "MyOrders", bundle: nil)
                let linkingVC = storyboard.instantiateViewController(withIdentifier: "MyOrders") as! PaymentURlsViewController
                linkingVC.orderresponse = self.orderresponse
                self.show(linkingVC, sender: self)
            }
        }
    }
    
    let carid = "\(MyCar.getCarID()!)"
    func CheckCar (){
        
            if self.carid == "-1"  {
               
                let alert = UIAlertController(title: "Alert", message: " please pick a car " , preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let linkingVC = storyboard.instantiateViewController(withIdentifier: "Container")
                        self.present(linkingVC, animated: true, completion: nil)
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
            }
            else {
    
            }
        
    }
    
    func dropShadow(){
        locationVIew.dropShadow()
        periodVIew.dropShadow()
        availableVIew.dropShadow()
        winshVIew.dropShadow()
        minServiceVIew.dropShadow()
        payview.dropShadow()
    }
    func setBorders(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = self.workshop.SreviceCars.count
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "servicesCell", for: indexPath)as! HomeServicesCollectionViewCell
        setBorders(view: cell.cellView)
        cell.cellView.dropShadow()
   //     cell.photo.image = self.logos[indexPath.row]
        cell.serviceName.text = self.workshop.SreviceCars[indexPath.row].Service_name
        cell.price.text = self.workshop.SreviceCars[indexPath.row].price + " KWD"
        cell.checkButton.tag = indexPath.row
        cell.checkButton.superview?.tag = indexPath.section
        cell.checkButton.addTarget(self, action: #selector(check(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(((collectionView.frame.size.width) - 30) / 3 ), height: CGFloat(130))
    }
    var select = false
    let servicesID = ""
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let cell = collectionView.cellForItem(at: indexPath) as! HomeServicesCollectionViewCell
        SubService.saveSubServiceID(servicceID: self.workshop.SreviceCars[indexPath.row].id)
        SubService.saveServiceName(servicceName: self.workshop.SreviceCars[indexPath.row].Service_name)
        self.select = !self.select
        if self.select{
            cell.checkButton.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
            showCustomDialog()
          //  workshop.SreviceCars[indexPath.row].id
           
        }
        else{
            cell.checkButton.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
    }
    
    
    func showCustomDialog(animated: Bool = true) {
        
        
        
        // Create a custom view controller
        let ratingVC = SubServicesViewController(nibName: "SubServiceTableViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            //         self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "Continue", height: 60) {
            SubServices.getSubServices()
          //  print(SubServices.getSubServices()!)
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
    @objc func check(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            sender.setImage(#imageLiteral(resourceName: "checked_checkbox_filled"), for: .normal)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "unchecked_checkbox_filled"), for: .normal)
        }
        
        
        
        
    }
    
    @IBOutlet weak var paymentView: UIView!
    @IBAction func paymentMethod(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            paymentView.isHidden = false
        }
        else {
             paymentView.isHidden = true
        }
    }
    

}
extension GarageViewController: UITextFieldDelegate {
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
