//
//  ActivitiesViewController.swift
//  CloudCar
//
//  Created by mac on 6/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        backButton.isHidden = true
    }
    @IBOutlet weak var backButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        let chck = FromSideMenu.getFromSideMenu()
        if chck != false {
            backButton.isHidden = false
        }
    }
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            let Invoice = self.storyboard?.instantiateViewController(withIdentifier: "Invoice")
            self.show(Invoice!, sender: self)
        }
    }

}
