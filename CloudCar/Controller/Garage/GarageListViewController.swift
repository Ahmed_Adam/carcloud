//
//  GarageListViewController.swift
//  CloudCar
//
//  Created by mac on 6/18/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class GarageListViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate{

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var workshops : WorkshopRoot!
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        GetGarage()
        let chck = FromSideMenu.getFromSideMenu()
        if chck != false {
            backButton.isHidden = false
        }
        
    }
 //   "service_id"] == "" || info["cat_ar_id"] == "" ||  info["cat_en_id"
    func GetGarage(){
        let info = ["servvar_id":"",
            "cat_ar_id":"",
            "cat_en_id":"",
            "4":"",
            "5":""
        ]
        ServicesAPI.sharedInstance.GetWorkShop(info: info) { (error , sucess , workshops) in
            self.workshops = workshops
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            
            // send this to the next view controller
        }
    }
    func setBorders(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workshops.data.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "garageListCell", for: indexPath) as! workshopCollectionViewCell
        let image = workshops.data[indexPath.row].image
       setBorders(view: cell.viewBorder)
       // cell.viewBorder.dropShadow()
        PhotoRequest.sharedInstance.getPhoto(url: image) { (error, success, image) in
            cell.workshopImage.image = image
        }
        cell.workshopName.text = workshops.data[indexPath.row].name
        cell.workshopAddress.text = workshops.data[indexPath.row].adress
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = workshops.data[indexPath.row].id
        WorkShop.saveWorkShop(workshop: id)
      //  WorkShop.saveJSON((json: workshops.data[indexPath.row], key:"selectedWorkshop"))
        

     //   let destination = GarageViewController(nibName: "GarageDetails", bundle: nil)
        
        let garage = self.storyboard?.instantiateViewController(withIdentifier: "GarageDetails")as! GarageViewController
        garage.workshop = workshops.data[indexPath.row]
        self.show(garage, sender: self)
    }

}
