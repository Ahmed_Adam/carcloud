//
//  AppDelegate.swift
//  CloudCar
//
//  Created by mac on 5/31/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import CoreLocation
import MOLH
import Fabric
import Crashlytics
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
var locationManager : CLLocationManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        MOLH.shared.activate(true)
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        UIApplication.shared.statusBarStyle = .lightContent
        
        GMSPlacesClient.provideAPIKey("AIzaSyDWWY7joAAb4zm2yxB05iIW3-2ao_FhWXQ")
        GMSServices.provideAPIKey("AIzaSyDWWY7joAAb4zm2yxB05iIW3-2ao_FhWXQ")
        let id = SaveUserID.getUserID()
        MyCar.saveCarID(ID: -1)
        Location.saveLat(Lat: "")
        Location.saveLong(Long: "")
        print(id as Any)
        
        let def = UserDefaults.standard
        if let user_token = def.object(forKey: "token"){
            print (user_token)
            
            let home = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "Container")
            window?.rootViewController = home
            
        }
        else {
            let first = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TheFirst")
            window?.rootViewController = first
            
        }
        
              Fabric.with([Crashlytics.self])
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "acf35e11-b98b-4db9-9ca1-39cfd2b9d3af",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        
        
        return true
    }
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "TheFirst")
    }


}

