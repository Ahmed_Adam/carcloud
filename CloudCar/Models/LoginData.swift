//
//  LoginData.swift
//  CloudCar
//
//  Created by mac on 6/24/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection


class LoginData: EVObject{
    
    var activeCode : String = ""
    var code : String = ""
    var createdAt : CreatedAt = CreatedAt()
    var deviceId : String = ""
    var email : String = ""
    var id : Int = -1
    var name : String = ""
    var telephone : String = ""
    var token : String = ""
    
    
}
