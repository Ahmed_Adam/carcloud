//
//  LoginResponse.swift
//  CloudCar
//
//  Created by mac on 6/24/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class LoginResponse: Codable{
    
    var check : Bool!
    var code : Int!
    var data : LoginData!
    var msg : Msg!
    
}
