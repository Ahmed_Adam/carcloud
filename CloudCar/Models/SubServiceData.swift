//
//  SubServiceData.swift
//  CloudCar
//
//  Created by mac on 7/9/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class SubServiceData : EVObject {
    
    
  //  var created_at : CreatedAt = CreatedAt()
    var description_ar : String = ""
    var description_en : String = ""
    var id : Int = -1
    var image_url : String = ""
    var name : String = ""
    
    
}
