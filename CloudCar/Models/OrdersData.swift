//
//  OrdersData.swift
//  CloudCar
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class OrdersData: EVObject{
    
   // var workshops : [AnyObject]!
    var car : Car = Car()
  //  var created_at  : CreatedAt = CreatedAt()
    var id : Int = -1
  //  var money : AnyObject!
    var pay : String = ""
    var price : Int = -1
    var service : [Service] = [Service]()
    var status : String = ""
    var user_id : UserId = UserId()
    var Workshops : [WorkShopData]!
    var payment_type_id : Int = -1
    var payment_type_name : String = ""
    var winch : String = ""
    var winch_date : String = ""
    var winch_lat : String = ""
    var winch_long : String = ""
    var image :String = ""
}
