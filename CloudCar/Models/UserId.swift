//
//  UserId.swift
//  CloudCar
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class UserId: EVObject{
    
    var activeCode : Int = -1
 //   var code : AnyObject!
  //  var createdAt : CreatedAt = CreatedAt()
    var deviceId : String = ""
    var email : String = ""
    var id : Int = -1
    var name : String = ""
    var telephone : String = ""
    var token : String = ""
    
    var code : String = ""

}
