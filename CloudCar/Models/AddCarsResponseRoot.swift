//
//  AddCarsResponseRoot.swift
//  CloudCar
//
//  Created by mac on 7/5/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class AddCarResponseRoot: EVObject{
    
    var check : Bool = false
    var code : Int = -1
    var data : [AddCarsData] = [AddCarsData] ()
    var msg : String = ""
    
    
}
