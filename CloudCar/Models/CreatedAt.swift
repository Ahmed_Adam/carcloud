//
//  CreatedAt.swift
//  CloudCar
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class CreatedAt: EVObject {
    
    var date : String = ""
    var timezone : String = ""
    var timezoneType : Int = -1
    
    
}
