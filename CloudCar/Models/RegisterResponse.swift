//
//  RegisterResponse.swift
//  CloudCar
//
//  Created by mac on 6/27/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import  EVReflection

class RegisterResponse: EVObject {
    
    var check : Bool = false
    var code : Int = 400
    var data : RegisterData = RegisterData()
    var msg : Msg = Msg()
    
}
