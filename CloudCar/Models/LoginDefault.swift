//
//  LoginDefault.swift
//  CloudCar
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class LoginResponse: EVObject {
    
    var check : Bool = false
    var code : Int = 400
    var data : LoginData = LoginData()
    var msg : Msg = Msg()
    
}
