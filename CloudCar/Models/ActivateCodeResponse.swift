//
//  ActivateCodeResponse.swift
//  CloudCar
//
//  Created by mac on 6/28/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import  EVReflection

class ActivateCodeResponse: EVObject {
    
    var check : Bool = false
    var code : Int = -1
    var data : ActivateCodeResponseData = ActivateCodeResponseData()


}

