//
//  DelegateClasses.swift
//  CloudCar
//
//  Created by mac on 6/26/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation

protocol stringToSendDelegate {
    func stringToSend(anyString: String)
}

