//
//  CheckLanguage.swift
//  Kalemat
//
//  Created by mac on 6/5/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
class CheckLanguage: NSObject {
    
    class func saveCheckLanguage (languge:String){
        let def = UserDefaults.standard
        def.setValue(languge , forKey: "languge")
        def.synchronize()
    }
    class  func getCheckLanguage ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "languge") as? String)
    }
    
}

